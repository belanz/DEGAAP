// swift-tools-version:6.0

import PackageDescription

let package = Package(
	name: "DEGAAP",
	platforms: [.iOS(.v17), .macOS(.v14), .tvOS(.v17), .watchOS(.v10)],
	products: [
		.library(
			name: "DEGAAP",
			targets: ["DEGAAP"])
	],
	dependencies: [
		.package(url: "https://github.com/robo-fish/CodableCSV", exact: "0.7.0"),
		.package(url: "https://gitlab.com/robo.fish/components/rftokenizablekeytree", from: "0.3.0"),
		.package(url: "https://gitlab.com/Kai.Oezer/IssueCollection", from: "0.5.2"),
		.package(url: "https://github.com/apple/swift-log", from: "1.6.1"),
		.package(url: "https://github.com/apple/swift-argument-parser", from: "1.5.0")
	],
	targets: [
		_libraryTarget,
		_commandLineToolTarget,
		_testTarget
	]
)

private var _libraryTarget : Target
{
	.target(
		name: "DEGAAP",
		dependencies: [
			.product(name: "CodableCSV", package: "CodableCSV"),
			.product(name: "RFTokenizableKeyTree", package: "rftokenizablekeytree"),
			.product(name: "IssueCollection", package: "IssueCollection"),
			.product(name: "Logging", package: "swift-log", condition: .when(platforms: [.linux, .android, .windows, .openbsd]))
		],
		resources: [.copy("Resources")],
		swiftSettings: [
			.define("DEBUG", .when(configuration: .debug)),
//			.define("CHART_ITEM_INCLUDES_LAW"),
			.define("CREATE_LINKBASE_FROM_KERNTAXONOMIE"),
		]
	)
}

private var _commandLineToolTarget : Target
{
	.executableTarget(
		name: "DEGAAPCLI",
		dependencies: [
			"DEGAAP",
			.product(name: "ArgumentParser", package: "swift-argument-parser")
		],
		swiftSettings: [
			.define("CREATE_LINKBASE_FROM_KERNTAXONOMIE")
		]
	)
}

private var _testTarget : Target
{
	.testTarget(
		name: "DEGAAPTests",
		dependencies: ["DEGAAP"],
		resources: [.copy("Resources")],
		swiftSettings: [
			.define("CREATE_LINKBASE_FROM_KERNTAXONOMIE")
		]
	)
}
