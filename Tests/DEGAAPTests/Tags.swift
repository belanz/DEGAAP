//  Copyright 2025 Kai Oezer

import Testing

extension Tag
{
	@Tag static var itemDescription : Self
	@Tag static var chartMapping : Self
	@Tag static var sourceChartItem : Self
	@Tag static var sourceChartGroup : Self
	@Tag static var sourceChartMapping : Self
	@Tag static var xbrlChart : Self
	@Tag static var xbrlTaxonomy : Self
	@Tag static var degaapItem : Self
	@Tag static var degaapEnvironment : Self
	@Tag static var linkbase : Self
}
