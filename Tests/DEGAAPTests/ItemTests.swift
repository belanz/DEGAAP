// Copyright 2021-2025 Kai Oezer

import Testing
@testable import DEGAAP

@Suite("DEGAAPItemID tests", .tags(.degaapItem))
struct ItemTests
{
	@Test("DEGAAP item ID creation")
	func createDEGAAPItemID() throws
	{
		#expect(DEGAAPItemID("valid.item") != nil)
		#expect(DEGAAPItemID("valid.item.with_underline") != nil)
		#expect(DEGAAPItemID("valid.item.with-dash-characters") != nil)
		#expect(DEGAAPItemID(".invalid.start") == nil)
		#expect(DEGAAPItemID("invalid.item.with.double..separator") == nil)
		#expect(DEGAAPItemID("invalid.item.with.??characters") == nil)
		#expect(DEGAAPItemID("invalid.item.with.+.*.characters") == nil)
		let itemID = try #require(DEGAAPItemID("hans.klaus.fritz.markus.anna.leon.nils.friedrich"))
		#expect(itemID.path.count == 8)
	}

	@Test("account types")
	func accountTypeChecks() throws
	{
		let id1 = try #require(DEGAAPItemID("is.netIncome.regular.operatingTC.otherOpRevenue.disposFixAss.sale.misc"))
		let id2 = try #require(DEGAAPItemID("bs.eqLiab.liab.shareholders.collateralised"))
		let id3 = try #require(DEGAAPItemID("bs.eqLiab.equity.subscribed.limitedLiablePartners.calledIn"))
		let id4 = try #require(DEGAAPItemID("bs.ass.currAss.securities.other.securities"))
		let id5 = try #require(DEGAAPItemID("bs.ass.fixAss.tan.other.leasedAss"))

		#expect(id5.isFixedAssetAccountID)
		#expect(id5.isAssetAccountID)
		#expect(id2.isFixedAssetAccountID == false)
		#expect(id4.isFixedAssetAccountID == false)
		#expect(id4.isCurrentAssetAccountID)
		#expect(id2.isCurrentAssetAccountID == false)
		#expect(id1.isCurrentAssetAccountID == false)
		#expect(id3.isEquityAccountID)
		#expect(id1.isEqLiabAccountID == false)
		#expect(id1.isEquityAccountID == false)
		#expect(id5.isEquityAccountID == false)
		#expect(id2.isLiabilityAccountID)
		#expect(id2.isEqLiabAccountID)
		#expect(id3.isLiabilityAccountID == false)
		#expect(id4.isLiabilityAccountID == false)
		#expect(id1.isIncomeOrCostAccountID)
		#expect(id3.isIncomeOrCostAccountID == false)
		#expect(id5.isIncomeOrCostAccountID == false)

		let id6 = try #require(DEGAAPItemID("bs.ass.currAss.receiv.other.vat"))
		let id7 = try #require(DEGAAPItemID("bs.eqLiab.liab.other.theroffTax"))
		#expect(id6.isAssetAccountID)
		#expect(id6.isReceivableTaxAccountID)
		#expect(id6.isPayableTaxAccountID == false)
		#expect(id6.isIncomeOrCostAccountID == false)
		#expect(id7.isEqLiabAccountID)
		#expect(id7.isPayableTaxAccountID)
		#expect(id7.isReceivableTaxAccountID == false)
		#expect(id7.isIncomeOrCostAccountID == false)
	}

	@Test("weights")
	func weights()
	{
		#expect(DEGAAP.DEGAAPWeight(string: "1") == .additive)
		#expect(DEGAAP.DEGAAPWeight(string: "-1") == .subtractive)
		#expect(DEGAAP.DEGAAPWeight(string: "10") == .neutral)
		#expect(DEGAAP.DEGAAPWeight(string: "-5") == .neutral)
		#expect(DEGAAP.DEGAAPWeight(string: "abc") == .neutral)
	}

	@Test("comparison")
	func comparison()
	{
		#expect(DEGAAPItemID("abra.cadabra")! < DEGAAPItemID("hokus.pokus")!)
		#expect(DEGAAPItemID("hokus.pokus")! < DEGAAPItemID("mumbo.jumbo")!)
	}

	@Test("coding")
	func coding() async throws
	{
		let accountName = "is.income.bla.bla"
		let encodedItemID = try #require(DEGAAPItemID(accountName))
		let archive = TestCodingArchive()
		let encoder = TestEncoder(archive: archive)
		try encodedItemID.encode(to: encoder)
		#expect(encoder.archive.data == accountName)

		let decoder = TestDecoder(archive: archive)
		let decodedItemID = try DEGAAPItemID(from: decoder)
		#expect (decodedItemID == encodedItemID)

		#expect(throws: DecodingError.self) {
			let invalidArchive = TestCodingArchive(data: "$@00!!!+")
			_ = try DEGAAPItemID(from: TestDecoder(archive: invalidArchive))
		}
	}
}
