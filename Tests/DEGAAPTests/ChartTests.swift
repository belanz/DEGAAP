//  Copyright 2021-2025 Kai Oezer

import Foundation
import Testing
@testable import DEGAAP

@Suite("Chart tests", .tags(.xbrlChart))
struct ChartTests
{
	@Test("loading a small GAAP chart")
	func loading() throws
	{
		let gaapFileLocation = try #require(Bundle.module.url(forResource: "mini-gaap-tax56", withExtension: "csv", subdirectory: "Resources"))
		let chart = try Chart(fileLocation: gaapFileLocation)
		let itemName = "bs.ass.currAss.receiv.other.vat"
		let itemID = try #require(DEGAAPItemID(itemName))
		let item = try #require(chart[itemID])
		#expect(chart[itemName] == item)
		#expect(chart["@!++%$"] == nil)
		#expect(item.definitionGerman == "Forderungen und sonstige Vermögensgegenstände, sonstige Vermögensgegenstände, Umsatzsteuerforderungen")
	}

	@Test("loading a nonexistent chart")
	func loadingNonexistentTable() async throws
	{
		let resourceRootLocation = try #require(Bundle.module.resourceURL)
		let bogusFileLocation = resourceRootLocation.appending(path: "bogusFile.csv", directoryHint: .notDirectory)
		#expect(throws: DEGAAPError.self) {
			let _ = try Chart(fileLocation: bogusFileLocation)
		}
	}

#if !CREATE_LINKBASE_FROM_KERNTAXONOMIE
	@Test("calculation arc linkbase")
	func calculationArcLinkbase() throws
	{
		let linkbase = try #require(XBRLLinkbase(from: Self.simpleLinkbase))
		#expect(linkbase.links.values.reduce(0){$0 + $1.count} == 10)
	}
#endif

#if !CREATE_LINKBASE_FROM_KERNTAXONOMIE
	static let simpleLinkbase =
	"""
	<?xml version="1.0" encoding="utf-8"?>
	<!--(c) XBRL Deutschland e.V. - see http://www.xbrl.de-->

	<linkbase xmlns="http://www.xbrl.org/2003/linkbase"
		xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
		xsi:schemaLocation="http://www.xbrl.org/2003/linkbase http://www.xbrl.org/2003/xbrl-linkbase-2003-12-31.xsd">
		<roleRef xmlns:xlink="http://www.w3.org/1999/xlink"
			roleURI="http://www.xbrl.de/taxonomies/de-gaap-ci/role/incomeStatement"
			xlink:href="de-gaap-ci-2020-04-01.xsd#role_incomeStatement"
			xlink:type="simple"/>
		<calculationLink xmlns:xlink="http://www.w3.org/1999/xlink"
			xlink:role="http://www.xbrl.de/taxonomies/de-gaap-ci/role/incomeStatement"
			xlink:type="extended">
			<loc xlink:type="locator"
				xlink:href="de-gaap-ci-2020-04-01.xsd#de-gaap-ci_is.netIncome.regular.operatingCOGS.inventoryChange"
				xlink:label="de-gaap-ci_is.netIncome.regular.operatingCOGS.inventoryChange"/>
			<calculationArc xlink:from="de-gaap-ci_is.netIncome.otherTaxes.personalTaxes"
				xlink:to="de-gaap-ci_is.netIncome.otherTaxes.personalTaxes.other"
				order="3"
				weight="1"
				xlink:arcrole="http://www.xbrl.org/2003/arcrole/summation-item"
				xlink:type="arc"/>
			<calculationArc xlink:from="de-gaap-ci_is.netIncome"
				xlink:to="de-gaap-ci_is.netIncome.incomeSharing"
				order="5"
				weight="1"
				xlink:arcrole="http://www.xbrl.org/2003/arcrole/summation-item"
				xlink:type="arc"/>
			<calculationArc xlink:from="de-gaap-ci_is.netIncome.incomeSharing"
				xlink:to="de-gaap-ci_is.netIncome.incomeSharing.loss"
				order="1"
				weight="1"
				xlink:arcrole="http://www.xbrl.org/2003/arcrole/summation-item"
				xlink:type="arc"/>
			<calculationArc xlink:from="de-gaap-ci_is.netIncome.incomeSharing.loss"
				xlink:to="de-gaap-ci_is.netIncome.incomeSharing.loss.profPooling"
				order="1"
				weight="1"
				xlink:arcrole="http://www.xbrl.org/2003/arcrole/summation-item"
				xlink:type="arc"/>
			<calculationArc xlink:from="de-gaap-ci_is.netIncome.incomeSharing.loss"
				xlink:to="de-gaap-ci_is.netIncome.incomeSharing.loss.profitTransfer"
				order="2"
				weight="1"
				xlink:arcrole="http://www.xbrl.org/2003/arcrole/summation-item"
				xlink:type="arc"/>
			<calculationArc xlink:from="de-gaap-ci_is.netIncome.incomeSharing.loss"
				xlink:to="de-gaap-ci_is.netIncome.incomeSharing.loss.partialProfitTransfer"
				order="3"
				weight="1"
				xlink:arcrole="http://www.xbrl.org/2003/arcrole/summation-item"
				xlink:type="arc"/>
			<calculationArc xlink:from="de-gaap-ci_is.netIncome.incomeSharing.loss"
				xlink:to="de-gaap-ci_is.netIncome.incomeSharing.loss.incomeTaxCreditAlloc"
				order="4"
				weight="1"
				xlink:arcrole="http://www.xbrl.org/2003/arcrole/summation-item"
				xlink:type="arc"/>
			<calculationArc xlink:from="de-gaap-ci_is.netIncome.incomeSharing"
				xlink:to="de-gaap-ci_is.netIncome.incomeSharing.gain"
				order="2"
				weight="-1"
				xlink:arcrole="http://www.xbrl.org/2003/arcrole/summation-item"
				xlink:type="arc"/>
			<calculationArc xlink:from="de-gaap-ci_is.netIncome.incomeSharing.gain"
				xlink:to="de-gaap-ci_is.netIncome.incomeSharing.gain.profPooling"
				order="1"
				weight="1"
				xlink:arcrole="http://www.xbrl.org/2003/arcrole/summation-item"
				xlink:type="arc"/>
			<calculationArc xlink:from="de-gaap-ci_is.netIncome.incomeSharing.gain"
				xlink:to="de-gaap-ci_is.netIncome.incomeSharing.gain.other"
				order="2"
				weight="1"
				xlink:arcrole="http://www.xbrl.org/2003/arcrole/summation-item"
				xlink:type="arc"/>
		 </calculationLink>
	</linkbase>
	"""
#endif
}
