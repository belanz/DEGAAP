// Copyright 2024-2025 Kai Oezer

import Testing
@testable import DEGAAP
import IssueCollection

@Suite("DEGAAPEnvironment tests", .tags(.degaapEnvironment))
struct EnvironmentTests
{
	let _environment : DEGAAPEnvironment

	init()
	{
		_environment = DEGAAPEnvironment()
	}

	@Test("loading all charts", .tags(.xbrlChart))
	func loadingAllCharts() async throws
	{
		let itemID1 = try #require(DEGAAPItemID("bs.eqLiab.equity.revenueRes.legal.forPresentYear"))
		let itemID2 = try #require(DEGAAPItemID("is.netIncome.regular.operatingTC.grossTradingProfit.totalOutput.netSales.grossSales"))
		for taxonomy in DEGAAPTaxonomyVersion.allCases {
			for fiscal in [true, false] {
				let chartDescriptor = DEGAAPChartDescriptor(taxonomy: taxonomy, fiscal: fiscal)
				for itemID in [itemID1, itemID2] {
					#expect(await _environment.chartItemExists(for: itemID, in: chartDescriptor))
//					#expect(requestedItem != nil, "Could not find expected chart item \"\(itemID)\" in chart loaded for taxonomy \(taxonomy).")
				}
			}
		}

		let hasFiscal6_3 = await _environment.contains(chart: .init(taxonomy: .v6_3, fiscal: true))
		#expect(hasFiscal6_3)
		let hasTrade6_5 = await _environment.contains(chart: .init(taxonomy: .v6_5, fiscal: false))
		#expect(hasTrade6_5)
	}

	@Test("chart item search", .tags(.xbrlChart))
	func chartItemSearch() async throws
	{
		let itemDescription = "is.netIncome.regular.operatingTC.deprAmort.currAss.receiv.other"
		let chart = DEGAAPChartDescriptor(taxonomy: .v6_5)
		let items = await _environment.chartItems(describedBy: itemDescription, in: chart, caseSensitive: true)
		#expect(items.count == 1)
		#expect(items[0].1 == "Abschreibungen (GKV), auf Vermögensgegenstände des Umlaufvermögens, soweit diese die in der Kapitalgesellschaft üblichen Abschreibungen überschreiten, Abschreibungen auf Forderungen und sonstige Vermögensgegenstände, übrige Abschreibungen auf Forderungen und sonstige Vermögensgegenstände")
		let itemDescription2 = "hokus pokus"
		let items2 = await _environment.chartItems(describedBy: itemDescription2, in: chart, caseSensitive: true)
		#expect(items2.isEmpty)
		let items3 = await _environment.chartItems(describedBy: "bs.ass.fixAss.tan.otherEquipm.passengerCars", in: chart)
		#expect(!items3.isEmpty)
		let items4 = await _environment.chartItems(describedBy: "Umsatzsteuerforderungen", in: chart)
		#expect(items4.count == 1)
		let items5 = await _environment.chartItems(describedBy: "umSATZsteuerFORDerungen", in: chart)
		#expect(items5.isEmpty)
		let items6 = await _environment.chartItems(describedBy: "umSATZsteuerFORDerungen", in: chart, caseSensitive: false)
		#expect(items6.count == 1)
	}

	@Test("description of chart item", .tags(.xbrlChart, .itemDescription))
	func chartItemDescription() async throws
	{
		let itemID = try #require(DEGAAPItemID("bs.ass.currAss.receiv.other.vat"))
		let descriptionResult = await _environment.description(for: itemID, chart: .init(taxonomy: .v6_4))
		let description = try #require(descriptionResult)
		#expect(description == "Forderungen und sonstige Vermögensgegenstände, sonstige Vermögensgegenstände, Umsatzsteuerforderungen")
	}

	@Test("chart item by description",
		.tags(.xbrlChart, .itemDescription),
		arguments: [
			("sonstige Vermögensgegenstände, Umsatzsteuerforderungen", "bs.ass.currAss.receiv.other.vat", DEGAAPTaxonomyVersion.v6_4)
		]
	)
	func chartItemByDescription(description : String, expectedItemDescription : String, taxonomy : DEGAAPTaxonomyVersion) async throws
	{
		let expectedItemID = try #require(DEGAAPItemID(expectedItemDescription))
		let searchResult = await _environment.chartItems(describedBy: description, in: .init(taxonomy: taxonomy))
		try #require(searchResult.isEmpty == false)
		#expect(searchResult[0].0 == expectedItemID)
	}

	@Test("taxonomy item linkbase", .tags(.xbrlChart))
	func taxonomyItemLinkbase() async throws
	{
		let taxonomyItemsAndWeights : [(String, DEGAAPWeight)] = [
			("is.netIncome.regular.operatingTC.staff.salaries", .subtractive),
			("some.bogus.account", .neutral),
			("is.netIncome.regular.fin.netParticipation.amortFinanc", .subtractive),
			("is.netIncome.tax.gewst", .subtractive),
			("is.netIncome.regular.fin.netInterest.income.misc", .additive)
		]
		let childItemIDs = taxonomyItemsAndWeights.map { DEGAAPItemID($0.0)! }
		let expectedWeights = taxonomyItemsAndWeights.map { $0.1 }
		let calculatedWeights = await _environment.weights(for: DEGAAPItemID("ismi.netIncome")!, children: childItemIDs, chart: .init(taxonomy: .v6_5, fiscal: true), section: .incomeStatement)
		#expect(calculatedWeights == expectedWeights)
	}

	@Test("chart item path", .tags(.xbrlChart))
	func itemPath() async throws
	{
		let chart = DEGAAPChartDescriptor(taxonomy: .v6_5, fiscal: true)
		let itemPath1 = await _environment.itemPath(for: DEGAAPItemID("is.netIncome.regular.operatingTC.otherCost.miscellaneous.misc")!, in: chart)
		#expect(itemPath1 == [])
		let itemPath2 = await _environment.itemPath(for: DEGAAPItemID("is.netIncome.regular.operatingTC.otherCost.miscellaneous")!, in: chart)
		#expect(itemPath2 == [
			DEGAAPItemID("ismi.netIncome")!,
			DEGAAPItemID("ismi.netIncome.otherCost")!,
			DEGAAPItemID("is.netIncome.regular.operatingTC.otherCost")!,
			DEGAAPItemID("is.netIncome.regular.operatingTC.otherCost.miscellaneous")!
		])
	}

	@Test("SKR03-to-DEGAAP mapping", .tags(.chartMapping))
	func skr03Mapping() async throws
	{
		for taxonomy in DEGAAPTaxonomyVersion.allCases {
			for fiscal in [true, false] {
				let chart = DEGAAPChartDescriptor(taxonomy: taxonomy, fiscal: fiscal)
				let mappingDescriptor = DEGAAPMappingDescriptor(source: .skr03, target: chart)
				let hasSKRMapping = await _environment.contains(mapping: mappingDescriptor)
				try #require(hasSKRMapping, "Failed verification that mapping is available for SKR 03 to DE-GAAP chart \(chart).")
				let account = await _environment.chartItem(for: DEGAAPSourceChartItemID("0030")!, mapping: mappingDescriptor)
				try #require(account != nil)
				#expect(account! == DEGAAPItemID("bs.ass.fixAss.intan.concessionBrands.licenses"))
				let account2 = await _environment.chartItem(for: DEGAAPSourceChartItemID("1080")!, mapping: mappingDescriptor)
				#expect(account2 == nil)
				let account3 = await _environment.chartItem(for: DEGAAPSourceChartItemID("4444")!, mapping: mappingDescriptor)
				#expect(account3 == nil)
				var issues = IssueCollection()
				await _environment.verifyMappedItems(for: mappingDescriptor, issues: &issues)
				#expect(issues.isEmpty, "Failed verification of mapping \(mappingDescriptor).")
			}
		}
	}

	@Test("SKR03-to-DEGAAP mapping search by description", .tags(.chartMapping, .itemDescription))
	func skr03DescriptionMapping() async throws
	{
		let chart = DEGAAPChartDescriptor(taxonomy: .v6_4, fiscal: true)
		let mapping = DEGAAPMappingDescriptor(source: .skr03, target: chart)
		let description1 = "Abziehbare Vorsteuer 7%"
		let skrItems1 = await _environment.sourceChartItems(describedBy: description1, mapping: mapping)
		#expect(skrItems1.count == 1)
		let skrItem1ID = try #require(skrItems1.first?.0)
		let skrItem1Result = await _environment.description(for: skrItem1ID, mapping: mapping)
		let skrItem1Description = try #require(skrItem1Result)
		#expect(skrItem1Description == description1)

		let skrItems2 = await _environment.sourceChartItems(describedBy: "Krötenwanderung", mapping: mapping)
		#expect(skrItems2.isEmpty)

		let skrItems3 = await _environment.sourceChartItems(describedBy: "abZIEHbare vorstEUER 7%", mapping: mapping, caseSensitive: false)
		#expect(skrItems3.count == 1)
		let skrItem3ID = try #require(skrItems3.first?.0)
		#expect(skrItem3ID == skrItem1ID)
	}

	@Test("IKR-to-DEGAAP mapping", .tags(.chartMapping))
	func ikrMappings() async throws
	{
		for taxonomyVersion in DEGAAPTaxonomyVersion.allCases {
			let chart = DEGAAPChartDescriptor(taxonomy: taxonomyVersion)
			let mappingDescr = DEGAAPMappingDescriptor(source: .ikr, target: chart)
			let itemResult = await _environment.chartItem(for: DEGAAPSourceChartItemID("0870")!, mapping: mappingDescr)
			let account = try #require(itemResult)
			#expect(account == DEGAAPItemID("bs.ass.fixAss.tan.otherEquipm.office")!)
			let item2Result = await _environment.chartItem(for: DEGAAPSourceChartItemID("6310")!, mapping: mappingDescr)
			if taxonomyVersion < .v6_8 {
				#expect(item2Result == nil)
			} else {
				#expect(item2Result == DEGAAPItemID("is.netIncome.regular.operatingTC.staff.salaries.misc")!)
			}
			var issues = IssueCollection()
			await _environment.verifyMappedItems(for: mappingDescr, issues: &issues)
			#expect(issues.isEmpty, "Failed verification of IKR mapping \(mappingDescr)")
		}
	}

	@Test("IKR description mapping", .tags(.chartMapping), arguments: DEGAAPTaxonomyVersion.allCases)
	func ikrDescriptionMapping(taxonomyVersion : DEGAAPTaxonomyVersion) async throws
	{
		let mappingDescr = DEGAAPMappingDescriptor(source: .ikr, target: .init(taxonomy: taxonomyVersion))
		let ikrItems1 = await _environment.sourceChartItems(describedBy: "Kasse", mapping: mappingDescr)
		#expect(ikrItems1.isEmpty == false)
		let ikrItems2 = await _environment.sourceChartItems(describedBy: "other", mapping: mappingDescr)
		#expect(ikrItems2.isEmpty)
	}

	@Test("fetching description from mapping", .tags(.chartMapping), arguments: DEGAAPTaxonomyVersion.allCases)
	func fetchingDescription(taxonomyVersion : DEGAAPTaxonomyVersion) async throws
	{
		let skrMapping = DEGAAPMappingDescriptor(source: .skr03, target: .init(taxonomy: taxonomyVersion))
		let itemID = try #require(DEGAAPSourceChartItemID("0840"))
		let skrItemResult = await _environment.chartItem(for: itemID, mapping: skrMapping)
		let skrItem = try #require(skrItemResult)
		#expect(skrItem == DEGAAPItemID("bs.eqLiab.equity.capRes")!)
		let descriptionResult = await _environment.description(for: itemID, mapping: skrMapping)
		let description = try #require(descriptionResult)
		#expect(description == "Kapitalrücklage")
	}

	@Test("Existing groups", .tags(.sourceChartGroup), arguments: [
		(DEGAAPSourceChart.ikr,   "2281", "Fertige Erzeugnisse und Waren", "Umlaufvermögen und aktive Rechnungsabgrenzung"),
		(DEGAAPSourceChart.ikr,   "7720", "Steuern vom Einkommen und Ertrag", "Weitere Aufwendungen"),
		(DEGAAPSourceChart.skr03, "2611", "Zinserträge", "Abgrenzungskonten"),
		(DEGAAPSourceChart.skr04, "5200", "Materialaufwand", "Betriebliche Aufwendungen")
	])
	func existingGroups(chart : DEGAAPSourceChart, accountID : String, expectedGroup : String, expectedCategory : String) async throws
	{
		let item = DEGAAPSourceChartItemID(accountID)!
		let (category, group) = try #require(await _environment.sourceChartGroup(for: item, in: chart))
		#expect(group == expectedGroup)
		#expect(category == expectedCategory)
	}

	@Test("Nonexisting groups",
		.tags(.sourceChartGroup),
		arguments: [
			(DEGAAPSourceChart.ikr, "5900"),
			(DEGAAPSourceChart.skr03, "2910"),
			(DEGAAPSourceChart.skr04, "7950")
		]
	)
	func nonexistingGroups(chart : DEGAAPSourceChart, accountID : String) async throws
	{
		let item = DEGAAPSourceChartItemID(accountID)!
		let (_, group) = try #require(await _environment.sourceChartGroup(for: item, in: chart))
		#expect(group == nil)
	}

	@Test("Finding chart items by source chart item ID", .tags(.sourceChartMapping))
	func findingChartItemsBySourceChartItemID() async throws
	{
		let skr04Mapping = DEGAAPMappingDescriptor(source: .skr04, target: .init(taxonomy: .v6_8, fiscal: true))
		let skr04Account_Fremdgeld = DEGAAPSourceChartItemID("1374")!
		let fetchedChartItem = await _environment.chartItem(for: skr04Account_Fremdgeld, mapping: skr04Mapping)
		let chartItem = try #require(fetchedChartItem)
		#expect(chartItem.description == "bs.ass.currAss.receiv.other.other")
		let itemDescription = await _environment.description(for: skr04Account_Fremdgeld, mapping: skr04Mapping)
		#expect(itemDescription == "Fremdgeld")
	}

	@Test("Source chart item descriptions",
		.tags(.itemDescription, .sourceChartMapping),
		arguments: DEGAAPSourceChart.allCases, DEGAAPTaxonomyVersion.allCases)
	func sourceItemDescriptions(chart : DEGAAPSourceChart, taxonomy : DEGAAPTaxonomyVersion) async throws
	{
		let itemsAndDescriptions = await _environment.sourceChartItemsWithDescription(for: chart, taxonomy: taxonomy)
		#expect(itemsAndDescriptions.isEmpty == false)
		if taxonomy == .v6_8 {
			if chart == .ikr {
				#expect(itemsAndDescriptions.count == 284)
			}
			if chart == .skr03 {
				#expect(itemsAndDescriptions.count == 1183)
			}
			if chart == .skr04 {
				#expect(itemsAndDescriptions.count == 1164)
			}
		}
	}

}
