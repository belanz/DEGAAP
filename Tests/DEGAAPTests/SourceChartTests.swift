//  Copyright 2025 Kai Oezer

import Testing
import Foundation
@testable import DEGAAP

@Suite
struct SourceChartTests
{
	@Test
	@available(macOS 13, iOS 16, tvOS 16, watchOS 9, *)
	func formatting()
	{
		let formatStyle = SourceChartTestFormatStyle()
		#expect(DEGAAPSourceChart.ikr.formatted(formatStyle) == "IKR")
		#expect(DEGAAPSourceChart.skr03.formatted(formatStyle) == "SKR03")
	}

	@Test
	@available(macOS 13, iOS 16, tvOS 16, watchOS 9, *)
	func parsing() throws
	{
		let strategy = SourceChartTestParseStrategy()
		#expect(try DEGAAPSourceChart("IKR", strategy: strategy) == .ikr)
		#expect(try DEGAAPSourceChart("SKR03", strategy: strategy) == .skr03)
		#expect(try DEGAAPSourceChart("SKR04", strategy: strategy) == .skr04)
		#expect(try DEGAAPSourceChart("BLA", strategy: strategy) == .ikr)
	}
}

struct SourceChartTestFormatStyle : ParseableFormatStyle
{
	func format(_ value : DEGAAPSourceChart) -> String
	{
		value.rawValue
	}

	var parseStrategy : SourceChartTestParseStrategy
	{
		SourceChartTestParseStrategy()
	}
}

struct SourceChartTestParseStrategy : ParseStrategy
{
	func parse(_ value : String) throws -> DEGAAPSourceChart
	{
		let lcValue = value.lowercased(with: .init(languageCode: .german, languageRegion: .germany))
		if lcValue.starts(with: "skr03") {
			return .skr03
		} else if lcValue.starts(with: "skr04") {
			return .skr04
		} else {
			return .ikr
		}
	}
}
