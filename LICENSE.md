## Urheberrechte und Benutzerlizenz<br/>_Copyright and User License_

Das Softwarepaket 'DEGAAP' wird vertrieben unter der folgenden BSD-Lizenz:

_The software library 'DEGAAP' is made available to you under the following BSD license:_

```
Copyright (c) 2020-2025 Kai Oezer

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice,
this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its
contributors may be used to endorse or promote products derived from
this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS “AS IS”
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
THE POSSIBILITY OF SUCH DAMAGE.
```


## Urheberrechte und Benutzerlizenzen für enthaltene Werke von Dritten<br/>_Copyrights and User Licenses for Third-Party Sources_

### XBRL Deutschland e.V.

Urheberrechte und Haftungsausschluss für die XBRL Kerntaxonomie-Versionen
6.4, 6.5 und 6.6, die in Teilen und bearbeiteter Form enthalten sind:

_Copyright notice and disclaimer for the XBRL taxonomies 6.4, 6.5, and 6.6
that are included partially and in modified form:_

```
Inhaber aller Rechte an dieser Taxonomie und der Begleitdokumentation ist der 
Verein XBRL Deutschland e.V., Düsseldorf. Weitere Rechte, insbesondere an der 
Marke „XBRL“ und an der technischen Spezifikation von XBRL, liegen bei 
XBRL International, Inc., New York, USA – zu Einzelheiten siehe
http://www.xbrl.org/legal.

XBRL Deutschland e.V. gestattet die freie Verwendung der Taxonomie und der 
Begleitdokumentation zu gewerblichen und nicht-gewerblichen Zwecken.

Die Taxonomie und die Begleitdokumente dürfen kopiert und an Dritte
weitergeben werden. Jede Kopie muss jedoch den Copyright-Vermerk aus dem
Original enthalten.

Die Taxonomie darf von Nutzern ergänzt, verkürzt oder in sonstiger Weise
geändert werden,  um deren speziellen Anforderungen zu dienen. Bearbeitete
Versionen müssen, wenn sie  an Dritte weitergegeben oder in irgendeiner Weise
in Verkehr gebracht werden,  als solche bezeichnet sein. Auch in bearbeiteten
Versionen darf der Original-Copyright-Vermerk nicht entfernt werden.

Als authentisch gilt im Zweifelsfall nur die Fassung der Taxonomie, die auf
den  Websites http://www.xbrl.de und http://www.xbrl.org hinterlegt ist.

Die Bereitstellung der Taxonomie und der Begleitdokumentation sowie deren
Herunterladen  und Verwendung führt nicht zu einem Vertrag zwischen XBRL
Deutschland e.V. und dem Nutzer. Insbesondere stellt die Bereitstellung
der Taxonomie und der Begleitdokumentation kein  Vertragsangebot von XBRL
Deutschland e.V. dar. XBRL Deutschland e.V. weist darauf hin, dass die
Taxonomie zur Verfügung gestellt wird, „wie sie ist“. Die Informationen
wurden  mit großer Sorgfalt durch fachkundige Personen erarbeitet. Dennoch
können Fehler nicht ausgeschlossen werden. Bei der Fülle des Materials
musste außerdem eine Auswahl der gängigsten und wichtigsten
Informationselemente getroffen werden, was in Einzelfällen den Eindruck der
Unvollständigkeit erwecken könnte. Generell ist zu berücksichtigen, dass die
Taxonomie stets nur die fachliche und technische Situation zum Zeitpunkt
ihrer Erstellung abbilden kann. Die Verwendung der Taxonomie erfolgt in
eigener Verantwortung des jeweiligen Nutzers. Weder XBRL Deutschland e.V.
noch die an der Entwicklung beteiligten Unternehmen, Institutionen und
Personen übernehmen eine Verantwortung für Vollständigkeit, Richtigkeit
und Gebrauchsfähigkeit der Taxonomie und der Begleitdokumentation.

Demzufolge ist die Haftung von XBRL Deutschland e.V. und der an der
Entwicklung beteiligten Unternehmen, Institutionen und Personen für Schäden,
die sich aus dem Herunterladen und der Nutzung der Taxonomie und der
Begleitdokumentation ergeben soweit rechtlich zulässig ausgeschlossen.
Insbesondere ist die Haftung für etwaige direkte oder indirekte Schäden
jeder Art, die aus dem Gebrauch der Taxonomie und der Begleitdokumentation 
erwachsen können, ausgeschlossen.
```

### *CodableCSV*

https://github.com/dehesa/CodableCSV

```
MIT License

Copyright (c) 2018 Marcos Sánchez-Dehesa

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
```
