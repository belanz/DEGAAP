//  Copyright 2021-2025 Kai Oezer

import Foundation

/// Represents an XBRL calculation arc with summation role.
struct CalculationArc : Hashable
{
	let itemID : DEGAAPItemID
	let weight : DEGAAPWeight

	init(to itemID : DEGAAPItemID, weight : DEGAAPWeight)
	{
		self.itemID = itemID
		self.weight = weight
	}

	init?(to item : String, weight : DEGAAPWeight)
	{
		guard let itemID = DEGAAPItemID(item) else { return nil }
		self.itemID = itemID
		self.weight = weight
	}

	func hash(into hasher: inout Hasher)
	{
		hasher.combine(itemID)
	}
}

typealias LinkMap = [DEGAAPItemID : Set<CalculationArc>]

protocol Linkbase : Equatable
{
	var reverse : any Linkbase { get }

	func joining(with linkbase : any Linkbase) -> any Linkbase

	func weights(for account : DEGAAPItemID, startingFrom startAccounts : [DEGAAPItemID]) -> [DEGAAPWeight]

	func path(for account : DEGAAPItemID) -> [DEGAAPItemID]
}

#if CREATE_LINKBASE_FROM_KERNTAXONOMIE

/// A link database extracted from the CSV-formatted DEGAAP taxonomy items table
struct TILinkbase : Linkbase
{
	private(set) var links = LinkMap()

	private init(linkmap : LinkMap)
	{
		self.links = linkmap
	}

	init(taxonomyItems : [TaxonomyItem], reportSection : TaxonomyItem.TIReportSection)
	{
		taxonomyItems.forEach { taxonomyItem in
			guard let itemID = taxonomyItem.id, taxonomyItem.reportSection == reportSection else { return }
			guard let parent = taxonomyItem.calcParent, let parentID = DEGAAPItemID(parent.itemName) else { return }
			let weight : DEGAAPWeight = taxonomyItem.calcWeight == nil ? .neutral : (taxonomyItem.calcWeight! == .plus ? .additive : .subtractive)
			self.links[itemID] = Set([CalculationArc(to: parentID, weight: weight)])
		}
	}

	var reverse : any Linkbase
	{
		return self
	}

	func joining(with linkbase : any Linkbase) -> any Linkbase
	{
		guard let tiLinkbase = linkbase as? TILinkbase else { return self }
		return TILinkbase(linkmap: links.merging(tiLinkbase.links, uniquingKeysWith: { (first, _) in first }))
	}

	func weights(for account : DEGAAPItemID, startingFrom startAccounts : [DEGAAPItemID]) -> [DEGAAPWeight]
	{
		startAccounts.map{ _cumulativeWeight(for: account, startingFrom: $0) }
	}

	private func _cumulativeWeight(for account : DEGAAPItemID, startingFrom startAccount : DEGAAPItemID) -> DEGAAPWeight
	{
		guard startAccount != account else { return .additive }
		guard let arc = self.links[startAccount]?.first else { return .neutral }
		guard arc.weight != .neutral else { return .neutral }
		return arc.weight * _cumulativeWeight(for: account, startingFrom: arc.itemID)
	}

	func path(for account : DEGAAPItemID) -> [DEGAAPItemID]
	{
		guard let startItem = self.links[account] else { return [] }
		var reversePath = [account]
		var item = startItem.first?.itemID
		while item != nil
		{
			reversePath.append(item!)
			item = links[item!]?.first?.itemID
		}
		return reversePath.reversed()
	}
}

#else // CREATE_LINKBASE_FROM_KERNTAXONOMIE

#if canImport(FoundationXML)
import FoundationXML
#endif

/// A link database extracted from XBRL-formatted DEGAAP linkbase files.
class XBRLLinkbase : NSObject, XMLParserDelegate, Linkbase
{
	private let _queue = DispatchQueue(label: "DEGAAP calculation arc linkbase access")
	private var _links = LinkMap()

	var links : LinkMap { _links }

	/// Provides account hierarchy link bases for the given taxonomy.
	///
	/// - returns: Tuple of three `Linkbase` instances that provide the account hierarchy links for
	/// the income statement, the income statement for small companies, and the balance sheet.
	static func linkbaseGroup(for taxonomy : DEGAAPTaxonomyVersion) throws -> (XBRLLinkbase, XBRLLinkbase, XBRLLinkbase)
	{
		let loadLinkbase : (String) throws -> XBRLLinkbase = { fileName in
			guard let location = Bundle.module.url(forResource: fileName, withExtension: "xml", subdirectory: "Resources") else { throw DEGAAPError.taxonomyLoading("Could not find the taxonomy data resource file.") }
			guard let linkbase = XBRLLinkbase(from: location) else { throw DEGAAPError.taxonomyLoading("Could not create a linkbase from the contents of the taxonomy data resource file.") }
			return linkbase
		}
		let releaseDate = taxonomy.releaseDateMarker
		let incomeStatementLinkbase = try loadLinkbase("de-gaap-ci-\(releaseDate)-calculation-incomeStatement")
		let incomeStatementLinkbaseMicroBilG = try loadLinkbase("de-gaap-ci-\(releaseDate)-calculation-incomeStatementMicroBilG")
		let balanceSheetLinkbase = try loadLinkbase("de-gaap-ci-\(releaseDate)-calculation-balanceSheet")
		return (incomeStatementLinkbase, incomeStatementLinkbaseMicroBilG, balanceSheetLinkbase)
	}

	/// Performs non-validating parsing of an XBRL linkbase file to extract its calculationArc entries.
	private init?(from xmlLinkbaseFileLocation : URL)
	{
		super.init()
		guard let parser = XMLParser(contentsOf: xmlLinkbaseFileLocation) else { return nil }
		parser.delegate = self
		guard parser.parse() else { return nil }
	}

	private init?(from xmlContents : String)
	{
		super.init()
		guard let xmlData = xmlContents.data(using: .utf8) else { return nil }
		let parser = XMLParser(data: xmlData)
		parser.delegate = self
		guard parser.parse() else { return nil }
	}

	init(links : LinkMap)
	{
		_links = links
	}

	func joining(with linkbase : any Linkbase) -> any Linkbase
	{
		guard let otherLinkbase = linkbase as? XBRLLinkbase else { return self }
		return XBRLLinkbase(links: _links.merging(otherLinkbase._links, uniquingKeysWith: { $0.union($1) }))
	}

	var reverse : any Linkbase
	{
		var linkbase : XBRLLinkbase?
		_queue.sync {
			var reverseLinks = LinkMap()
			_links.forEach { (fromItem, forwardLinks) in
				forwardLinks.forEach {
					reverseLinks[$0.itemID] = Set([CalculationArc(to: fromItem, weight: $0.weight)]).union(reverseLinks[$0.itemID] ?? [])
				}
			}
			linkbase = XBRLLinkbase(links: reverseLinks)
		}
		return linkbase!
	}

	func weights(for account : DEGAAPItemID, startingFrom startAccounts : [DEGAAPItemID]) -> [DEGAAPWeight]
	{
		startAccounts.map { _cumulativeWeight(for: account, startingFrom: $0) }
	}

	func path(for account : DEGAAPItemID) -> [DEGAAPItemID]
	{
		return []
	}

	// XMLParserDelegate

	func parser(_ parser: XMLParser, didStartElement elementName: String, namespaceURI: String?, qualifiedName qName: String?, attributes : [String : String] = [:])
	{
		if elementName == "calculationArc"
		{
			guard let role = Self._valueForAttribute(named: "xlink:arcrole", in: attributes),
				role == "http://www.xbrl.org/2003/arcrole/summation-item" else { return }
			guard let from = Self._valueForAttribute(named: "xlink:from", in: attributes, removingPrefix: "de-gaap-ci_") else { return }
			guard let to = Self._valueForAttribute(named: "xlink:to", in: attributes, removingPrefix: "de-gaap-ci_") else { return }
			guard let weightStr = Self._valueForAttribute(named: "weight", in: attributes) else { return }
			let weight = DEGAAPWeight(string: weightStr)
			// guard let orderStr = _valueForAttribute(named: "order", in: attributePtr) else { return nil }
			guard let origin = DEGAAPItemID(from), let arc = CalculationArc(to: to, weight: weight) else { return }
			_queue.sync {
				_links[origin] = Set([arc]).union(_links[origin] ?? [])
			}
		}
	}

	func parser(_ parser: XMLParser, parseErrorOccurred parseError: Error)
	{
		assertionFailure("XML parser encountered error \(parseError)")
	}

	// -----------------------

	static private func _valueForAttribute(named name : String, in attributes : [String:String], removingPrefix prefix : String? = nil) -> String?
	{
		guard let value = attributes[name] else { return nil }
		if (prefix != nil) && value.hasPrefix(prefix!)
		{
			return String(value[value.index(value.startIndex, offsetBy: prefix!.count)...])
		}
		return value
	}

	private func _cumulativeWeight(for account : DEGAAPItemID, startingFrom startAccount : DEGAAPItemID) -> DEGAAPWeight
	{
		guard startAccount != account else { return .additive }
		guard let arcs = _links[startAccount] else { return .neutral }
		let pathWeights : [DEGAAPWeight] = arcs.map {
			guard $0.weight != .neutral else { return .neutral }
			return $0.weight * _cumulativeWeight(for: account, startingFrom: $0.itemID)
		}
		return pathWeights.filter { $0 != .neutral }.first ?? .neutral
	}

}

#endif // CREATE_LINKBASE_FROM_KERNTAXONOMIE

func == (lhs : any Linkbase, rhs : any Linkbase) -> Bool
{
#if CREATE_LINKBASE_FROM_KERNTAXONOMIE
	guard let rhsTI = rhs as? TILinkbase, let lhsTI = lhs as? TILinkbase else { return false }
	return lhsTI.links == rhsTI.links
#else
	return false
#endif
}

/* XML parsing with libxml2 instead of FoundationXML. libxml2 is not available on Linux, however.

#else

import libxml2

/// Stores calculation relationships between DEGAAP items.
struct CalculationLinkbase
{
	private let _queue = DispatchQueue(label: "DEGAAP calculation linkbase access")
	private let _arcs : Set<CalculationArc>

	/// Performs non-validating parsing of an XBRL linkbase file to extract its calculationArc entries.
	init?(from xmlLinkbaseFileLocation : URL)
	{
		let options : Int32 = 0
		var document : UnsafeMutablePointer<xmlDoc>!
		xmlLinkbaseFileLocation.absoluteString.withCString { url in
			document = xmlReadFile(url, nil, options)
		}
		guard document != nil else { return nil }
		defer { xmlFreeDoc(document) }
		_arcs = Self._extractCalculationArcs(traversing: document)
	}

	init?(from xmlContents : String)
	{
		_arcs = xmlContents.withCString { ptr in
			let document = xmlReadMemory(ptr, Int32(xmlContents.count), "noname.xml", nil, 0)
			defer { xmlFreeDoc(document) }
			return Self._extractCalculationArcs(traversing: document)
		}
	}

	private init(arcs : Set<CalculationArc>)
	{
		_arcs = arcs
	}

	func withArcs(do operation : (Set<CalculationArc>)->() )
	{
		_queue.sync { operation(_arcs) }
	}

	func joining(with otherLinkbase : CalculationLinkbase) -> CalculationLinkbase
	{
		var result : CalculationLinkbase?
		otherLinkbase.withArcs{ result = CalculationLinkbase(arcs: $0.union(self._arcs)) }
		precondition(result != nil)
		return result!
	}

	static private func _extractCalculationArcs(traversing document : UnsafePointer<xmlDoc>!) -> Set<CalculationArc>
	{
		var calcLinks = Set<CalculationArc>()
		if document != nil, let root = xmlDocGetRootElement(document)
		{
			Self._extractCalculationArcs(into: &calcLinks, traversing: root)
		}
		return calcLinks
	}

	static private func _extractCalculationArcs(
		into arcs : inout Set<CalculationArc>,
		traversing node : UnsafeMutablePointer<xmlNode>!)
	{
		var cur_node : UnsafeMutablePointer<xmlNode>! = node

		while cur_node != nil
		{
			if cur_node.pointee.type == XML_ELEMENT_NODE
			{
				if xmlStrcmp(cur_node.pointee.name, "calculationArc") == 0
				{
					if let arc = _extractSummationArc(cur_node)
					{
						arcs.insert(arc)
					}
				}
				else
				{
					_extractCalculationArcs(into: &arcs, traversing: cur_node.pointee.children)
				}
			}
			cur_node = cur_node.pointee.next
		}
	}

	static private func _extractSummationArc(_ arcNode : UnsafeMutablePointer<xmlNode>!) -> CalculationArc?
	{
		let attributePtr = arcNode.pointee.properties
		precondition(attributePtr != nil)
		guard let role = _valueForAttribute(named: "arcrole", in: attributePtr),
			role == "http://www.xbrl.org/2003/arcrole/summation-item" else { return nil }
		guard let from = _valueForAttribute(named: "from", in: attributePtr, removingPrefix: "de-gaap-ci_") else { return nil }
		guard let to = _valueForAttribute(named: "to", in: attributePtr, removingPrefix: "de-gaap-ci_") else { return nil }
		guard let weightStr = _valueForAttribute(named: "weight", in: attributePtr),
			let weight = Float(weightStr) else { return nil }
		// guard let orderStr = _valueForAttribute(named: "order", in: attributePtr) else { return nil }
		return CalculationArc(from: from, to: to, weight: Float(weight))
	}

	static private func _valueForAttribute(named name : String,
		in arcPropertyPtr : UnsafeMutablePointer<xmlAttr>!,
		removingPrefix prefix : String? = nil) -> String?
	{
		var nextProperty = arcPropertyPtr
		while nextProperty != nil
		{
			if xmlStrcmp(nextProperty!.pointee.name, name) == 0
			{
				if let valuePtr = xmlNodeGetContent(nextProperty!.pointee.children)
				{
					let value = String(cString: valuePtr)
					if prefix != nil && value.starts(with: prefix!)
					{
						return String(value[value.index(value.startIndex, offsetBy: prefix!.count)...])
					}
					return value
				}
			}
			nextProperty = nextProperty!.pointee.next
		}
		return nil
	}

}

#endif
*/
