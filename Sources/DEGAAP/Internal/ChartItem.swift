//  Copyright 2020-2024 Kai Oezer

import RFTokenizableKeyTree

/// Stores the data for an XBRL DE-GAAP accounts chart item.
struct ChartItem : Codable, Sendable
{
	let id : DEGAAPItemID
	let section : DEGAAPReportSection
	let balanceType : BalanceType
	let definition : String
	let definitionGerman : String
	let requirement : ReportingRequirement?
#if CHART_ITEM_INCLUDES_LAW
	let associatedLaws : Set<Law>?
#endif

#if CHART_ITEM_INCLUDES_LAW
	init(id : DEGAAPItemID,
		section : DEGAAPReportSection,
		balanceType : DEGAAPBalanceType,
		definition : String = "",
		definitionGerman : String = "",
		requirement : DEGAAPReportingRequirement? = nil,
		associatedLaws : Set<Law>? = nil)
	{
		self.id = id
		self.section = section
		self.balanceType = balanceType
		self.definition = definition
		self.definitionGerman = definitionGerman
		self.associatedLaws = associatedLaws
		self.requirement = requirement
	}
#else
	init(id : DEGAAPItemID,
		section : DEGAAPReportSection,
		balanceType : BalanceType,
		definition : String = "",
		definitionGerman : String = "",
		requirement : ReportingRequirement? = nil)
	{
		self.id = id
		self.section = section
		self.balanceType = balanceType
		self.definition = definition
		self.definitionGerman = definitionGerman
		self.requirement = requirement
	}
#endif
}

#if !CHART_ITEM_INCLUDES_LAW

typealias ExpandableChartItem = ChartItem

extension ChartItem : Hashable
{
	public func hash(into hasher: inout Hasher)
	{
		hasher.combine(id)
	}

	public static func == (lhs: ChartItem, rhs: ChartItem) -> Bool
	{
		lhs.id == rhs.id
	}
}

extension ChartItem : RFTokenizableKeyItem
{
	public var tokenizableKey : DEGAAPItemID { id }
}

extension ChartItem : CustomStringConvertible
{
	public var description: String { "\(id) : \(definition)" }
}

#else

/// A reference type that contains a ``ChartItem`` whose properties can be modified.
/// This type is for use in internal data structures. It eliminates the need to create a copy of the
/// whole data structure just because one `ChartItem` instance needs to be modified.
final class ExpandableChartItem
{
	private(set) var item : ChartItem

	init(item : ChartItem)
	{
		self.item = item
	}

	func add(law newLaw : ChartItem.Law)
	{
		let newLaws = item.associatedLaws?.union([newLaw]) ?? [newLaw]
		item = ChartItem(
			id: item.id,
			section: item.section,
			balanceType: item.balanceType,
			periodType: item.periodType,
			valueType: item.valueType,
			definition: item.definition,
			definitionGerman: item.definitionGerman,
			requirement: item.requirement,
			associatedLaws: newLaws)
	}
}

extension ExpandableChartItem : Hashable
{
	public func hash(into hasher: inout Hasher)
	{
		hasher.combine(item.id)
	}

	public static func == (lhs: ExpandableChartItem, rhs: ExpandableChartItem) -> Bool
	{
		lhs.item.id == rhs.item.id
	}
}

extension ExpandableChartItem : RFTokenizableKeyItem
{
	public var tokenizableKey : DEGAAPItemID { item.id }
}

extension ExpandableChartItem : CustomStringConvertible
{
	public var description: String { "\(item.id) : \(item.definition)" }
}

#endif
