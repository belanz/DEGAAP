// Copyright 2024-2025 Kai Oezer

import RegexBuilder

public typealias DEGAAPSourceChartCategoryID = Int

/// A DEGAAP source chart item is identified by a four-character string representing four digits.
public struct DEGAAPSourceChartItemID : Sendable
{
	public let code : String

	private let _numericalValue : Int

	public init?(_ code : String)
	{
		guard Self._validate(code) else { return nil }
		guard let numericValue = Int(code), numericValue >= 0 else { return nil }
		self.code = code
		_numericalValue = numericValue
	}

	/// The account category (a.k.a. account class) that the item belongs to.
	///
	/// German: Kontenklasse
	public var accountCategory : DEGAAPSourceChartCategoryID { _numericalValue / 1000 }

#if true
	public static var pattern : Regex<Substring> { /^\d{4}$/ }
#else
	public static var pattern : Regex<Regex<(Substring, Regex<Repeat<Substring>.RegexOutput>.RegexOutput)>.RegexOutput> {
		Regex {
			Anchor.startOfLine
			Capture {
				Repeat(count: 4) {
					CharacterClass(.digit)
				}
			}
			Anchor.endOfLine
		}
	}
#endif

	private static func _validate(_ code : String) -> Bool
	{
		do { return try Self.pattern.firstMatch(in: code) != nil }
		catch { return false }
	}
}

extension DEGAAPSourceChartItemID
{
	public static let none = DEGAAPSourceChartItemID("0000")!
}

extension DEGAAPSourceChartItemID : CustomStringConvertible
{
	public var description : String
	{
		self == .none ? "" : code
	}
}

extension DEGAAPSourceChartItemID : Hashable
{
	public func hash(into hasher: inout Hasher)
	{
		hasher.combine(_numericalValue)
	}
}

extension DEGAAPSourceChartItemID : Comparable
{
	public static func < (lhs: DEGAAPSourceChartItemID, rhs: DEGAAPSourceChartItemID) -> Bool
	{
		lhs._numericalValue < rhs._numericalValue
	}

	public static func > (lhs: DEGAAPSourceChartItemID, rhs: DEGAAPSourceChartItemID) -> Bool
	{
		lhs._numericalValue > rhs._numericalValue
	}
}

extension DEGAAPSourceChartItemID : Equatable
{
	public static func == (lhs: DEGAAPSourceChartItemID, rhs: DEGAAPSourceChartItemID) -> Bool
	{
		lhs._numericalValue == rhs._numericalValue
	}
}

extension DEGAAPSourceChartItemID : Decodable
{
	public init(from decoder: Decoder) throws
	{
		let container = try decoder.singleValueContainer()
		let content = try container.decode(String.self)
		guard let itemID = Self.init(content) else {
			throw DecodingError.dataCorruptedError(in: container, debugDescription: "Invalid DEGAAP source chart item ID \"\(content)\"")
		}
		self = itemID
	}
}
