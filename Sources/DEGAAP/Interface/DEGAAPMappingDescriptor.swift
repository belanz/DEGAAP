// Copyright 2024 Kai Oezer

public struct DEGAAPMappingDescriptor : Sendable, Codable
{
	public let source : DEGAAPSourceChart
	public let target : DEGAAPChartDescriptor

	public init(source : DEGAAPSourceChart, target : DEGAAPChartDescriptor)
	{
		self.source = source
		self.target = target
	}
}

extension DEGAAPMappingDescriptor : Hashable
{
	public func hash(into hasher: inout Hasher)
	{
		hasher.combine(source)
		hasher.combine(target)
	}
}
