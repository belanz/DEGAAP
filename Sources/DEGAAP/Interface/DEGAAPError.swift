//  Copyright 2020-2023 Kai Oezer

import Foundation

public enum DEGAAPError : Error
{
	case taxonomyLoading(String)
}
