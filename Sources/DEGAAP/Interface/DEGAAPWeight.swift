// Copyright 2021-2024 Kai Oezer

public enum DEGAAPWeight : Int, Codable, Sendable
{
	case subtractive = -1
	case neutral = 0
	case additive = 1

	init(string : String)
	{
		guard let intRepr = Int(string) else { self = .neutral; return }
		self = (intRepr == 1 ? .additive : (intRepr == -1 ? .subtractive : .neutral))
	}

	static func * (lhs : DEGAAPWeight, rhs : DEGAAPWeight) -> DEGAAPWeight
	{
		DEGAAPWeight(rawValue: lhs.rawValue * rhs.rawValue)!
	}
}
