//  Copyright 2024 Kai Oezer

import Foundation
import IssueCollection

/// This class is the thread-safe, public interface to the functions of ``DEGAAP``.
public actor DEGAAPEnvironment
{
	private var _charts = [DEGAAPChartDescriptor : Chart]()

	/// stores direct mappings from source charts to DE-GAAP charts
	private var _mappings = [DEGAAPMappingDescriptor : Mapping]()

	/// stores indirect mappings from source charts to source charts that map to DE-GAAP charts
	private var _sourceMappings = [DEGAAPSourceChart : SourceMapping]()

	/// stores the account categories and account groups of the source charts.
	private var _sourceGroups = [DEGAAPSourceChart : [DEGAAPSourceChartCategory]]()

	public init()
	{
	}

	/// - Returns: `true` if a DE-GAAP item with the given item ID exists in the chart for the given descriptor, `false` otherwise.
	public func chartItemExists(for itemID : DEGAAPItemID, in chartDescr : DEGAAPChartDescriptor) -> Bool
	{
		_chart(for: chartDescr)?[itemID] != nil
	}

	/// - Returns: The DE-GAAP item ID for the given source chart item ID in the mapping table for the given mapping descriptor , or `nil` if no item could be found.
	public func chartItem(for itemID : DEGAAPSourceChartItemID, mapping mappingDescr : DEGAAPMappingDescriptor) -> DEGAAPItemID?
	{
		if let mapping = _mapping(for: mappingDescr) {
			return mapping[itemID]
		}
		let skr03MappingDescr = DEGAAPMappingDescriptor(source: .skr03, target: mappingDescr.target)
		guard let sourceMapping = _sourceMapping(for: mappingDescr.source),
			let skr03Item = sourceMapping[itemID],
			let skr03Mapping = _mapping(for: skr03MappingDescr)
			else { return nil }
		return skr03Mapping[skr03Item]
	}

	/// Searches in chart tables for items whose description contains the given search key.
	///
	/// - Returns: A list of pairs of chart item ID and description, where the description contains the given parameter `searchKey`.
	public func chartItems(describedBy searchKey : String, in chartDescr : DEGAAPChartDescriptor, caseSensitive : Bool = true) -> [(DEGAAPItemID, String)]
	{
		let chart = _chart(for: chartDescr)
		assert(chart != nil)
		return chart?.items(describedBy: searchKey, caseSensitive: caseSensitive) ?? []
	}

	/// - Returns: Sorted list of all source chart items paired with their descriptions,
	/// retrieved from the mapping table for the given source chart and given taxonomy.
	public func sourceChartItemsWithDescription(for sourceChart : DEGAAPSourceChart, taxonomy : DEGAAPTaxonomyVersion) -> [(DEGAAPSourceChartItemID, String)]
	{
		let descriptor = DEGAAPMappingDescriptor(source: sourceChart, target: .init(taxonomy: taxonomy))
		let itemsWithDescription = _mapping(for: descriptor)?.itemsWithDescription ?? (_sourceMapping(for: sourceChart)?.itemsWithDescription ?? [])
		assert(itemsWithDescription.isEmpty == false)
		return itemsWithDescription.sorted { $0.0 < $1.0 }
	}

	/// Searches for chart mapping source items matching the given description.
	///
	/// - Returns: A list of source chart item IDs paired with their descriptions,
	/// where either the item ID or the description contains the given search key.
	/// - Parameters:
	///   - searchKey: A string that, for a successful match, needs to occur at least once in the item description.
	///   - mapping: The mapping descriptor that determines which table will be searched.
	///   - caseSensitive: Whether the match is required to be case sensitive.
	///
	///  If the mapping from the given source chart to the DE-GAAP chart for the given taxonomy
	///  is an indirect mapping, involving an intermediate source chart mapping, the search for a match
	///  in the description is first done in the source chart mapping, and falls back to the description
	///  in the DE-GAAP mapping if there is no description in the source chart mapping.
	public func sourceChartItems(describedBy searchKey : String, mapping mappingDescr : DEGAAPMappingDescriptor, caseSensitive : Bool = true) -> [(DEGAAPSourceChartItemID, String)]
	{
		if let mapping = _mapping(for: mappingDescr) {
			return mapping.items(describedBy: searchKey, caseSensitive: caseSensitive)
		}
		if let sourceMapping = _sourceMapping(for: mappingDescr.source) {
			let itemsWithDescriptionsFromSourceChart = sourceMapping.items(describedBy: searchKey, caseSensitive: caseSensitive)
			let skr03Mapping = _mapping(for: DEGAAPMappingDescriptor(source: .skr03, target: mappingDescr.target))
			assert(skr03Mapping != nil, "Could not find a mapping from SKR 03 to \(mappingDescr.target)")
			return itemsWithDescriptionsFromSourceChart.compactMap { (itemID, itemDescription) in
				guard let description = itemDescription ?? skr03Mapping?.description(for: itemID) else { return nil }
				return (itemID, description)
			}
		}
		return []
	}

	/// - Returns: The description for the given chart item ID, from the chart for the given chart descriptor, or `nil` if the item could not be found in the table.
	/// - Parameters:
	///   - itemID: The chart item ID associated with the description.
	///   - chartDescr: The chart descriptor that determines which chart will be searched.
	///   - language: The preferred language of the description.
	public func description(for itemID : DEGAAPItemID, chart chartDescr : DEGAAPChartDescriptor, language : Locale.LanguageCode = .german) -> String?
	{
		guard let chart = _chart(for: chartDescr),
			let item = chart[itemID]
			else { return nil }
		return language == .german ? item.definitionGerman : item.definition
	}

	/// - Returns: The description for the given source chart item ID, from the mapping table for the given mapping descriptor, or `nil` if the item could not be found in the table.
	/// - Parameters:
	///   - itemID: The source chart item ID associated with the description.
	///   - mapping: The mapping descriptor that determines which mapping table will be searched.
	///   - language: The preferred language of the description.
	public func description(for itemID : DEGAAPSourceChartItemID, mapping mappingDescr : DEGAAPMappingDescriptor, language : Locale.LanguageCode = .german) -> String?
	{
		if let mapping = _mapping(for: mappingDescr) {
			return mapping.descriptionMap[itemID]
		}
		if let sourceMapping = _sourceMapping(for: mappingDescr.source) {
			return sourceMapping.description(for: itemID)
		}
		return nil
	}

	/// Searches the chart, given by its descriptor, for DEGAAP ancestor accounts.
	/// - Returns: The ancestor accounts, ordered from the highest level down to the given item, or `nil` if a chart was not found.
	/// - Parameter itemID: The DE-GAAP item ID for the chart item whose ancestor accounts ("Oberpositionen") will be searched.
	/// - Parameter chartDescr: The descriptor for the chart to search for ancestor accounts.
	///
	/// If the given item was found in the chart, it will be included as the last item in the returned list.
	/// If the given item is not part of the chart, an empty list will be returned.
	public func itemPath(for itemID : DEGAAPItemID, in chartDescr : DEGAAPChartDescriptor) -> [DEGAAPItemID]?
	{
		guard let chart = _chart(for: chartDescr) else { return nil }
		return chart.itemPath(for: itemID)
	}

	/// - Returns: The weights (-1, 0, +1) with which the given child items contribute to the
	/// - Parameters:
	///   - item: The higher-level parent item, to which the given child items will be backtracked.
	///   - childItems: The items to be backtracked to the parent item.
	///   - chartDescr: descriptor of the chart to be used
	///   - section: The chart section to use for backtracking.
	/// given parent item, or `nil` if a chart was not found for the given taxonomy.
	///
	/// For a child item to contribute positively or negatively to the given parent item,
	/// * the parent must be reachable from the child item in the inverse linkbase;
	/// * the periodType (duration or instant) must match;
	/// * parent and child must have the same balance type, i.e. matching credit/debit properties.
	public func weights(
		for item : DEGAAPItemID,
		children childItems : [DEGAAPItemID],
		chart chartDescr : DEGAAPChartDescriptor,
		section : DEGAAPReportSection
	) -> [DEGAAPWeight]?
	{
		guard let chart = _chart(for: chartDescr) else { return nil }
		return chart.weights(for: item, children: childItems, section: section)
	}

	/// Checks whether a chart for the given descriptor exists.
	/// If the chart is not loaded yet, it attempts to load the chart before returning.
	///
	/// - Returns: Whether the given chart is available.
	public func contains(chart chartDescr : DEGAAPChartDescriptor) -> Bool
	{
		return _chart(for: chartDescr) != nil
	}

	/// Checks whether a chart mapping for the given descriptor exists.
	/// If the mapping is not loaded yet, it attempts to load the mapping before returning.
	///
	/// - Returns: Whether the given mapping is available.
	public func contains(mapping mappingDescr : DEGAAPMappingDescriptor) -> Bool
	{
		(_mapping(for: mappingDescr) != nil) || (_sourceMapping(for: mappingDescr.source) != nil)
	}

	/// - Returns: The category name and group name, respectively,
	/// for the source chart item with the given ID in the given source chart.
	/// Returns `nil` if no category was found for the given item.
	/// Returns a pair with `nil` group name if no group was found for the given item.
	public func sourceChartGroup(for itemID : DEGAAPSourceChartItemID, in chart : DEGAAPSourceChart) -> (String, String?)?
	{
		if _sourceGroups[chart] == nil {
			_sourceGroups[chart] = loadSourceChartGroups(for: chart)
		}

		guard let categories = _sourceGroups[chart],
			let category = categories.first(where: {$0.id == itemID.accountCategory})
			else { return nil }
		let groupDescription = category.groups.first(where: {$0.range.contains(itemID)})?.description
		return (category.description, groupDescription)
	}

	private func _chart(for chartDescriptor : DEGAAPChartDescriptor) -> Chart?
	{
		if let existingChart = _charts[chartDescriptor] {
			return existingChart
		}
		if let newChart = try? Chart(taxonomy: chartDescriptor.taxonomy, fiscal: chartDescriptor.fiscal) {
			_charts[chartDescriptor] = newChart
			return newChart
		}
		return nil
	}

	/// - Returns: An existing or newly constructed (if mapping data is available) `Mapping` instance for the given mapping descriptor, or `nil` if there was an error.
	private func _mapping(for mappingDescr : DEGAAPMappingDescriptor) -> Mapping?
	{
		let effectiveMapping = _effectiveMapping(for: mappingDescr)
		if let existingMapping = _mappings[effectiveMapping] {
			return existingMapping
		}
		guard let newMapping = try? Mapping(descriptor: effectiveMapping) else { return nil }
		_mappings[effectiveMapping] = newMapping
		return newMapping
	}

	/// - Returns: An existing or newly constructed `SourceMapping` instance for the given source chart type, `nil` if there was an error.
	private func _sourceMapping(for sourceChart : DEGAAPSourceChart) -> SourceMapping?
	{
		if let existingSourceMapping = _sourceMappings[sourceChart] {
			return existingSourceMapping
		}
		guard let newSourceMapping = SourceMapping(type: sourceChart) else { return nil }
		_sourceMappings[sourceChart] = newSourceMapping
		return newSourceMapping
	}

	private func _effectiveMapping(for mappingDescr : DEGAAPMappingDescriptor) -> DEGAAPMappingDescriptor
	{
		let effectiveTaxonomy : DEGAAPTaxonomyVersion = {
			if mappingDescr.source == .ikr {
				if (mappingDescr.target.taxonomy >= .v6_8) {
					return .v6_8
				}
				if (mappingDescr.target.taxonomy >= .v6_5) {
					return .v6_5
				}
			} else {
				if (mappingDescr.target.taxonomy > .v6_7) {
					return .v6_7
				}
			}
			return mappingDescr.target.taxonomy
		}()
		return DEGAAPMappingDescriptor(source: mappingDescr.source, target: DEGAAPChartDescriptor(taxonomy: effectiveTaxonomy, fiscal: mappingDescr.target.fiscal))
	}

}

extension DEGAAPEnvironment
{
	func verifyMappedItems(for mappingDescr : DEGAAPMappingDescriptor, issues : inout IssueCollection)
	{
		guard let chart = _chart(for: mappingDescr.target) else {
			issues.append(Issue(domain: .degaap, code: .missingChartData))
			return
		}
		guard let mapping = _mapping(for: mappingDescr) else {
			issues.append(Issue(domain: .degaap, code: .missingMappingData))
			return
		}
		Self.verifyMappedItems(in: mapping, using: chart, issues: &issues)
	}
	
	static func verifyMappedItems(in mapping : Mapping, using chart : Chart, issues : inout IssueCollection)
	{
		mapping.chartMap.forEach { (sourceItem, degaapItemID) in
			guard let chartItem = chart[degaapItemID] else {
				issues.append(Issue(domain: .degaap, code: .mappedItemMissingFromChart, metadata: [.sourceItem : sourceItem.description, .mappedItem : degaapItemID.description]))
				return
			}
			if let req = chartItem.requirement {
				if req == .requiredSummation {
					issues.append(Issue(domain: .degaap, code: .mappedItemIsSummationItem, metadata: [.sourceItem : sourceItem.description, .mappedItem : degaapItemID.description]))
				}
			}
		}
	}
}

extension IssueDomain
{
	static var degaap : IssueDomain { "DEGAAP" }
}

extension IssueCode
{
	static var missingChartData             : IssueCode {  1 } /// chart data could not be loaded
	static var missingMappingData           : IssueCode {  2 } /// mapping data could not be loaded
	static var mappedItemMissingFromChart   : IssueCode { 10 } /// a mapped DEGAAP item is missing in the chart
	static var mappedItemIsSummationItem    : IssueCode { 11 } /// a mapped DEGAAP item is a summation item (Summenmussfeld)
}

extension IssueMetadataKey
{
	static var sourceItem : IssueMetadataKey { "sourceItem" }
	static var mappedItem : IssueMetadataKey { "mappedItem" }
}
