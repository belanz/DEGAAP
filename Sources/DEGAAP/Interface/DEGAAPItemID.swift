//  Copyright 2020-2025 Kai Oezer

import RFTokenizableKeyTree
import RegexBuilder

/**
	The DE-GAAP-CI XBRL tag based on German HGB (Handelsgesetzbuch),
	similar to U.S. GAAP (Generally Accepted Accounting Principles).
*/
public struct DEGAAPItemID : Codable, Hashable, Comparable, Equatable, CustomStringConvertible, Sendable
{
	public let name : String
	public let path : [String]

	public enum Error : Swift.Error { case invalidID }

	public init?(_ name : String)
	{
		guard Self._isValidGAAPName(name) else {
			logger.debug("initialization with invalid GAAP name: \(name)")
			return nil
		}
		self.name = name
		self.path = Self._path(for: name)
	}

	public init(from decoder: Decoder) throws
	{
		let container = try decoder.singleValueContainer()
		let value = try container.decode(String.self)
		guard Self._isValidGAAPName(value) else {
			throw DecodingError.dataCorruptedError(in: container, debugDescription: "Invalid DEGAAP item ID \"\(value)\"")
		}
		self.name = value
		self.path = Self._path(for: value)
	}

	public func encode(to encoder: Encoder) throws
	{
		var container = encoder.singleValueContainer()
		try container.encode(name)
	}

	private static func _path(for value : String) -> [String]
	{
		value.components(separatedBy: ".")
	}

	private static func _isValidGAAPName(_ name : String) -> Bool
	{
		/// [ICU regular expressions documentation](https://unicode-org.github.io/icu/userguide/strings/regexp.html)
	#if false
		let gaapNamePattern = /^[A-Za-z0-9_-]++(?:\.[A-Za-z0-9_-]++)*+$/
	#else
		let sectionCharacters = Regex {
			OneOrMore
			{
				ChoiceOf
				{
					CharacterClass("A"..."Z", "a"..."z", "0"..."9")
					"-"
					"_"
				}
			}
		}
		let gaapNamePattern = Regex {
			Anchor.startOfLine
			sectionCharacters
			ZeroOrMore
			{
				"."
				sectionCharacters
			}
			Anchor.endOfLine
		}
	#endif
		return name.wholeMatch(of: gaapNamePattern) != nil
	}

	public static func < (lhs: DEGAAPItemID, rhs: DEGAAPItemID) -> Bool
	{
		lhs.name.lowercased() < rhs.name.lowercased()
	}

	public static func == (lhs: DEGAAPItemID, rhs: DEGAAPItemID) -> Bool
	{
		lhs.name == rhs.name
	}

	public var description : String { name }
}

extension DEGAAPItemID : RFTokenizableKey
{
	public var tokens : [String] { path }
}

extension DEGAAPItemID
{
	public var isAssetAccountID : Bool         { name.starts(with: "bs.ass.")          }
	public var isFixedAssetAccountID : Bool    { name.starts(with: "bs.ass.fixAss.")    }
	public var isCurrentAssetAccountID : Bool  { name.starts(with: "bs.ass.currAss.")   }
	public var isEqLiabAccountID : Bool        { name.starts(with: "bs.eqLiab.")       }
	public var isEquityAccountID : Bool        { name.starts(with: "bs.eqLiab.equity.") }
	public var isLiabilityAccountID : Bool     { name.starts(with: "bs.eqLiab.liab.")   }
	public var isIncomeOrCostAccountID : Bool  { name.starts(with: "is.netIncome.")     }

	public var isReceivableTaxAccountID : Bool { name == "bs.ass.currAss.receiv.other.vat" }
	public var isPayableTaxAccountID : Bool    { name == "bs.eqLiab.liab.other.theroffTax" }
}
