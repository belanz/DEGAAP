//  Copyright 2024 Kai Oezer

public struct DEGAAPChartDescriptor : Codable, Hashable, Sendable
{
	public let taxonomy : DEGAAPTaxonomyVersion
	public let fiscal : Bool

	public init(taxonomy: DEGAAPTaxonomyVersion, fiscal : Bool = true)
	{
		self.taxonomy = taxonomy
		self.fiscal = fiscal
	}
}

extension DEGAAPChartDescriptor : CustomStringConvertible
{
	public var description: String
	{
		"\(taxonomy)-" + (fiscal ? "fiscal" : "trade")
	}
}
