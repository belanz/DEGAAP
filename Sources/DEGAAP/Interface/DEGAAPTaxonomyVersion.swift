// Copyright 2021-2024 Kai Oezer

import Foundation

public enum DEGAAPTaxonomyTrait : Sendable
{
	/// the BVV (Betriebsvermögensvergleich) section is required
	case bvv
}

/// Version der HGB-Taxonomie
public enum DEGAAPTaxonomyVersion : Int, Codable, CaseIterable, Hashable, Sendable
{
	case v6_3 = 603
	case v6_4 = 604
	case v6_5 = 605
	case v6_6 = 606
	case v6_7 = 607
	case v6_8 = 608
}

extension DEGAAPTaxonomyVersion : Comparable, Equatable
{
	public static func < (lhs : Self, rhs : Self) -> Bool
	{
		lhs.rawValue < rhs.rawValue
	}

	public static func > (lhs : Self, rhs : Self) -> Bool
	{
		lhs.rawValue > rhs.rawValue
	}

	public static func == (lhs : Self, rhs : Self) -> Bool
	{
		lhs.rawValue == rhs.rawValue
	}
}

extension DEGAAPTaxonomyVersion : CustomStringConvertible
{
	public var description : String
	{
		"\(self.rawValue/100)_\(self.rawValue % 100)"
	}
}

extension DEGAAPTaxonomyVersion
{
	public init?(date : Date)
	{
		let year = Calendar(identifier: .gregorian).component(.year, from: date)
		switch year
		{
			case ...2020 : self = .v6_3
			case 2021    : self = .v6_4
			case 2022    : self = .v6_5
			case 2023    : self = .v6_6
			case 2024    : self = .v6_7
			case 2025    : self = .v6_8
			default      : return nil
		}
	}

	/// - Returns: The taxonomy versions allowed to be used in reports for the given business year.
	/// - Parameter businessYear: The calendar year of the business report.
	///
	/// It is assumed that the business year coincides with the calendar year.
	///
	/// Taxonomien gelten für bestimmte Berichtsjahre. Ein Aufstellung der
	/// veröffentlichten Taxonomien gegenüber den Wirtschaftsjahren für dessen
	/// Abschlüsse sie verwendet werden dürfen findet man in einem PDF unter
	/// https://www.esteuer.de
	///   → In "Inhaltsübersicht" links " Schnittstellen zur E-Bilanz"
	///   → im Text nach dem Link "Übersicht" suchen
	///
	/// Erklärtext zur Übersichtstabelle aus esteuer.de :
	/// "Eine Eröffnungsbilanz ist mit einer der Taxonomieversionen zu übermitteln, mit
	/// der auch die Schlussbilanz des ersten Wirtschaftsjahres übermittelt werden kann.
	/// Dabei ist abzustellen auf die Angaben zum Wirtschaftsjahr im GCD-Modul.
	/// Es wird nicht beanstandet, wenn eine Eröffnungsbilanz mit der vorhergehenden
	/// Taxonomieversion übermittelt wird.
	/// Bei den Bilanzarten „Zwischenabschluss“, „unterjährige Zahlen“, „Umwandlungsbilanz“
	/// und „Liquidationsanfangsbilanz“ wird es nicht beanstandet, wenn sie mit der
	/// vorhergehenden Taxonomieversion übermittelt werden."
	public static func allowedTaxonomies(businessYear : Int) -> [DEGAAPTaxonomyVersion]
	{
		switch businessYear
		{
			case 2019: return [.v6_3]
			case 2020: return [.v6_3, .v6_4]
			case 2021: return [.v6_4, .v6_5]
			case 2022: return [.v6_5, .v6_6]
			case 2023: return [.v6_6, .v6_7]
			case 2024: return [.v6_7, .v6_8]
			case 2025: return [.v6_8]
			default: return []
		}
	}

	public var traits : [DEGAAPTaxonomyTrait]
	{
		(self > .v6_3) ? [.bvv] : []
	}

	// used for locating the resource files for a taxonomy
	public var releaseDateMarker : String
	{
		switch self
		{
			case .v6_3 : return "2019-04-01"
			case .v6_4 : return "2020-04-01"
			case .v6_5 : return "2021-04-14"
			case .v6_6 : return "2022-05-02"
			case .v6_7 : return "2023-04-01"
			case .v6_8 : return "2024-04-01"
		}
	}
}

extension DEGAAPTaxonomyVersion
{
	public var humanReadable : String
	{
		"\(self.rawValue/100).\(self.rawValue % 100)"
	}

	public init?(fromHumanReadable input : String)
	{
		let tokens = input.components(separatedBy: ".")
		guard tokens.count == 2,
			let major = Int(tokens[0]),
			let minor = Int(tokens[1]),
			let decodedTaxonomy = DEGAAPTaxonomyVersion(rawValue: major * 100 + minor)
			else { return nil }
		self = decodedTaxonomy
	}
}
