0xxx Immaterielle Vermögensgegenstände
0200-0299 Konzessionen, gewerbliche Schutzrechte und ähnliche Rechte und Werte sowie Lizenzen an solchen Rechten und Werte
0300-0399 Geschäfts- oder Firmenwert
0500-0599 Grundstücke, grundstücksgleiche Rechte und Bauten einschließlich der Bauten auf fremden Grundstücken
0700-0799 Technische Anlagen und Maschinen
0800-0899 Andere Anlagen, Betriebs- und Geschäftsausstattung
0900-0999 Geleistete Anzahlungen und Anlagen im Bau

1xxx Finanzanlagen
1300-1399 Beteiligungen
1500-1599 Wertpapiere des Anlagevermögens
1600-1699 Sonstige Finanzanlagen

2xxx Umlaufvermögen und aktive Rechnungsabgrenzung
2000-2099 Roh-, Hilfs- und Betriebsstoffe
2100-2199 Unfertige Erzeugnisse, unfertige Leistungen
2200-2299 Fertige Erzeugnisse und Waren
2300-2399 Geleistete Anzahlungen auf Vorräte
2400-2499 Forderungen aus Lieferungen und Leistungen
2500-2599 Innergemeinschaftlicher Erwerb/Einfuhr
2600-2699 Sonstige Vermögensgegenstände
2700-2799 Wertpapiere des Umlaufvermögens
2800-2899 Flüssige Mittel
2900-2999 Aktive Rechnungsabgrenzung (und Bilanzfehlbetrag)

3xxx Eigenkapital und Rückstellungen
3000-3099 Eigenkapital/Gezeichnetes Kapital
3100-3199 Kapitalrücklage
3200-3299 Gewinnrücklagen
3300-3399 Ergebnisverwendung
3400-3499 Jahresüberschuss/Jahresfehlbetrag
3500-3599 Sonderposten mit Rücklageanteil
3600-3699 Wertberichtigungen
3700-3799 Rückstellungen für Pensionen und ähnliche Verpflichtungen
3800-3899 Steuerrückstellungen
3900-3999 Sonstige Rückstellungen

4xxx Verbindlichkeiten und passive Rechnungsabgrenzung
4100-4199 Anleihen
4200-4299 Verbindlichkeiten gegenüber Kreditinstituten
4300-4399 Erhaltene Anzahlungen auf Bestellungen
4400-4499 Verbindlichkeiten aus Lieferungen und Leistungen
4800-4899 Sonstige Verbindlichkeiten
4900-4999 Passive Rechnungsabgrenzung

5xxx Erträge
5000-5099 Umsatzerlöse für eigene Erzeugnisse und andere eigene Leistungen
5100-5199 Umsatzerlöse für Waren und sonstige Umsatzerlöse
5200-5299 Erhöhung oder Verminderung des Bestandes an unfertigen und fertigen Erzeugnissen
5300-5399 Andere aktivierte Eigenleistungen
5400-5499 Sonstige betriebliche Erträge
5500-5599 Erträge aus Beteiligungen
5600-5699 Erträge aus anderen Wertpapieren u. Ausleihungen des Finanzanlagevermögens
5700-5799 Sonstige Zinsen und ähnliche Erträge

6xxx Betriebliche Aufwendungen
6000-6099 Aufwendungen für Roh-, Hilfs- und Betriebsstoffe und für bezogene Waren
6100-6199 Aufwendungen für bezogene Leistungen
6200-6299 Löhne
6300-6399 Gehälter
6400-6499 Soziale Abgaben und Aufwendungen für Altersversorgung und für Unterstützung
6500-6599 Abschreibungen
6600-6699 Sonstige Personalaufwendungen
6700-6799 Aufwendungen für die Inanspruchnahme von Rechten und Diensten
6800-6899 Aufwendungen für Kommunikation (Dokumentation, Information, Reisen, Werbung)
6900-6999 Aufwendungen für Beiträge und Sonstiges sowie Wertkorrekturen und periodenfremde Aufwendungen

7xxx Weitere Aufwendungen
7000-7099 Betriebliche Steuern
7400-7499 Abschreibungen auf Finanzanlagen und auf Wertpapiere des Umlaufvermögens und Verluste aus entsprechenden Abgängen
7500-7599 Zinsen und ähnliche Aufwendungen
7700-7799 Steuern vom Einkommen und Ertrag
7800-7899 Diverse Aufwendungen

8xxx Ergebnisrechnungen 
8000-8099 Eröffnung/Abschluss
8100-8199 Herstellungskosten
8200-8299 Vertriebskosten
8300-8399 Allgemeine Verwaltungskosten
8400-8499 Sonstige betriebliche Aufwendungen
8500-8599 Korrekturkonten zu den Erträgen der Kontenklasse 5
8600-8699 Korrekturkonten zu den Aufwendungen der Kontenklasse 6
8700-8799 Korrekturkonten zu den Aufwendungen der Kontenklasse 7
8800-8899 Kurzfristige Erfolgsrechnung (KER)
8900-8999 Innerjährige Rechnungsabgrenzung

9xxx Kosten- und Leistungsrechnung (KLR)
9000-9099 Unternehmensbezogene Abgrenzungen
9100-9199 Kostenrechnerische Korrekturen
9200-9299 Kostenarten und Leistungsarten
9300-9399 Kostenstellen
9400-9499 Kostenträger
9500-9599 Fertige Erzeugnisse
9600-9699 Interne Lieferungen und Leistungen sowie deren Kosten
9700-9799 Umsatzkosten
9800-9899 Umsatzleistungen
9900-9999 Ergebnisausweise
