// Copyright 2023 Kai Oezer

import Foundation
import DEGAAP
import ArgumentParser

@main
struct DEGAAPParsableCommand : ParsableCommand
{
	static var _commandName : String { "degaap" }

	static let configuration = CommandConfiguration(
		abstract: "Searches the specified information in the DEGAAP tables.",
		subcommands: [SearchCommand.self],
		defaultSubcommand: SearchCommand.self
	)
}

extension DEGAAPParsableCommand
{
	struct Options : ParsableArguments
	{
		@Option(name: .shortAndLong, help: "The DE-GAAP XBRL taxonomy to use for the search. Taxonomy version 6.5 is the default.")
		var taxonomy : DEGAAPTaxonomyVersion = .v6_5

		@Flag(name: .shortAndLong, help: "Whether to load the fiscal taxonomy table or the trade taxonomy table.")
		var fiscal : Bool = false
		
		@Argument(help: "The XBRL account name. Example: is.netIncome.regular.operatingTC.otherCost.energyCost")
		var account : DEGAAPItemID
	}
}

extension DEGAAPTaxonomyVersion : ExpressibleByArgument
{
	public init?(argument: String)
	{
		self.init(fromHumanReadable: argument)
	}
}

extension DEGAAPItemID : ExpressibleByArgument
{
	public init?(argument: String)
	{
		self.init(argument)
	}
}
