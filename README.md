# DEGAAP

This repository provides simple utility functions and data structures for working with DE-GAAP accounts,
which is the XBRL-based business reporting standard for companies in Germany.

A small set of the DE-GAAP taxonomy files, published by [XBRL Deutschland e.V.](https://de.xbrl.org/),
are included, in original or modified form.

DEGAAP maintains only the "MicroBilG" flavored XBRL account charts, applicable to very small corporations (Kleinstkapitalgesellschaft).

## Taxonomy version

DEGAAP supports various taxonomy versions. The taxonomy version that needs to be specified when using
the DEGAAP functions depends on the business year for which the report is being prepared.

| year | taxonomy |
|------|-----|
| 2019 | 6.3 |
| 2020 | 6.4 |
| 2021 | 6.4 | 
| 2022 | 6.5 |
| 2023 | 6.6 |
| 2024 | 6.7 |
| 2025 | 6.8 |

## Architecture Overview

![DEGAAP structure](Documentation/structure.svg)

### *Chart* vs *Source Chart*

In DEGAAP nomenclature, "chart" refers to an XBRL DE-GAAP chart that lists DE-GAAP account names and their for some taxonomy version. "Source chart" refers to the commonly used, non-XBRL accounts charts SKR 03, SKR 04, and IKR. The functions in DEGAAPEnvironment are named accordingly 

### Account chart type

For each supported taxonomy version, DEGAAP supports two types of account charts: trade and fiscal.

## Taxonomy Data Preparation

The XBRL taxonomy CSV files included in DEGAAP were prepared by
- downloading the release package from https://de.xbrl.org;
- picking the "de-gaap-ci-...-microbilg.xlsx" Microsoft Excel files, applicable for small companies, from the _Exceldokumentation/gaap_ folder;
- also picking the "de-gcd-...-shell.xlsx" file from the _Exceldokumentation/gcd_ folder;
- opening the XLSX files in Apple _Numbers_ for editing;
- removing the first 13 columns, designated "Review-Spalten";
- removing the first 6 rows, up to (not including) the row that actually contains the column names ("abstract", "balance" etc.);
- removing all columns except
  - in section "Konzepteigenschaften",
    - abstract
    - balance
    - periodType
    - type
    - name
    - Berichtsteil
    - +/-
    - Rechnerisch verknüpft mit
    - lvl
  - in section "Bezeichner",
    - standard de
    - standard en
    - terse de
    - terse en
    - documentation de
    - definitionGuidance de
  - in section "Referenzen",
    - name (rename to "law name")
    - paragraph (rename to "law paragraph")
    - subparagraph (rename to "law subparagraph")
    - notPermittedFor
    - fiscalRequirement
    - typeOperatingResult
    - legalFormEU
    - legalFormPG
    - legalFormKSt
- in the _microbilg_ documents, removing rows where the column "Berichtsteil" contains
  - "Steuerliche Gewinnermittlung für besondere Fälle"
- in the _gcd_ documents, adding the missing column "typeOperatingResult", leaving it empty;
- fixing misspelled DE-GAAP names with consecutive dot (.) characters;
  - "dim_parentDRS22P.accGroupStatement.reserves.revenueRes..other"
- exporting the resulting table as a __semicolon-separated__, UTF-8 encoded CSV file, not including the table names.
  - In order to make Numbers export a semicolon-separated CSV, open the System Settings app, go to _General_ → _Language & Region_, and choose the number format "_1.234.567,89_".

### SKR 03 Mapping

Starting with XBRL taxonomy version 6.7, the mapping from DATEV SKR&nbsp;03 accounts to DE-GAAP accounts
has been prepared based on [DATEV SKR&nbsp;03 E-Bilanz für Kapital&shy;gesell&shy;schaft](https://www.datev.de/web/de/datev-shop/material/skr-03-e-bilanz-fuer-kapitalgesellschaft/),
in which the corresponding _E-Bilanz_ accounts are indicated, with minor modifications.
